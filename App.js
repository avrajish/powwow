import React from 'react';
import Expo from 'expo';

import { Platform, StatusBar, ActivityIndicator } from 'react-native';
import { StackNavigator } from 'react-navigation';

import LoginScreen from './components/LoginScreen';
import UserListScreen from './components/UserListScreen';
import ChatScreen from './components/ChatScreen';

const Navigator = StackNavigator(
  {
    LoginScreen: {
      screen: LoginScreen,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },
    UserListScreen: {
      screen: UserListScreen,
      navigationOptions: ({ navigation }) => ({
        title: 'Contacts'
      })
    },
    ChatScreen: {
      screen: ChatScreen,
      navigationOptions: ({ navigation }) => ({
        title: navigation.state.params.title
      })
    }
  },
  {
    cardStyle: {
      paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight
    }
  }
);

export default class App extends React.Component {
  constructor(props) {
    console.disableYellowBox = true;

    super(props);

    this.state = {
      isReady: false
    }
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
    });

    this.setState({
      isReady: true
    });
  }

  render() {
    if(this.state.isReady) {
      return (
        <Navigator />
      );
    }
    return <ActivityIndicator />
  }
}
