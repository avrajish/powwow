import React, { Component } from 'react';
import PubNubReact from 'pubnub-react';
import KeyValueStorage from 'react-native-key-value-storage'
import { KeyboardAvoidingView, AsyncStorage, Text,ScrollView, View, Button, FlatList, StyleSheet, Image, Alert, TextInput } from 'react-native';

import { Platform } from 'react-native';
import { Container } from 'native-base';

import KeyboardSpacer from 'react-native-keyboard-spacer';
import { GiftedChat } from 'react-native-gifted-chat';

const GATEWAY_URI = 'http://satdevv713.athenahealth.com:38888';
const chatHistoryDate = '';

export default class ChatPage extends Component {

  constructor(props) {


    super(props);
    this.state = {
      message: '',
      chatHistory: [],
      messages: [],
    };


    AsyncStorage.getAllKeys((err, keys) => {
      AsyncStorage.multiGet(keys, (err, stores) => {

        this.subscribeKey = stores.find(i => i[0] === 'subscribeKey')[1];
        this.publishKey = stores.find(i => i[0] === 'publishKey')[1];
        this.cipherKey = stores.find(i => i[0] === 'cipherKey')[1];
        this.token = stores.find(i => i[0] === 'token')[1];
        this.pubnubUserId = stores.find(i => i[0] === 'pubnubUserId')[1];

        this.pubnub = new PubNubReact({
          subscribeKey: this.subscribeKey,
          publishKey: this.publishKey,
          cipherKey: this.cipherKey,
          authKey: this.token,
          logVerbosity: false,
          uuid: this.pubnubUserId,
          ssl: true,
          presenceTimeout: 130
        });

        this.stores = stores;

        this.pubnub.addListener(this._getPubnubListeners());
        console.log("Subscribing..");
        this.pubnub.subscribe({
          channelGroups: [this.pubnubUserId + '-channelgroup'],
          withPresence: true
        });

        this.getUserChatHistory(props.navigation.state.params.grpChatId);

      });
    });


    this._getPubnubListeners = this._getPubnubListeners.bind(this);
    this.getUserChatHistory = this.getUserChatHistory.bind(this);

    // this.bindMsgToUI = this.bindMsgToUI.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
  }

  // Each of these listeners should be implemented properly as to handle the event changes.
  // TODO: Also figure out if it should be better to move this to a different file
  _getPubnubListeners() {
    return pubnubListeners = {
      status: (statusEvent) => {
        console.groupCollapsed('Pubnub: status handler');
        console.log("Status:" + statusEvent.category);
        if (statusEvent.category === "PNConnectedCategory") {
          //console.log("Connected!");
        }
        if (statusEvent.error) {
          //console.log("Status error:"+statusEvent.error);
        };
        console.groupEnd();
      },
      message: (message) => {
        console.groupCollapsed('Pubnub: message handler');
        // TODO: Implementation
        console.log("New Message!!", message);
        var newChatHistory = this.state.chatHistory.slice();
        newChatHistory.push({
          timetoken: message.timetoken,
          sender: message.message.sender,
          message: message.message.content.message
        });
        this.setState({
          chatHistory: newChatHistory
        });
        console.log(this.state);
        console.groupEnd();

        this.getUserChatHistory(this.props.navigation.state.params.grpChatId);
      },
      presence: function (event) {
        console.groupCollapsed('Pubnub: presence handler');
        console.log(event);
        var action = event.action;
        var channel = event.channel;
        var uuid = event.uuid;
        var data = event.state;
        var occupancy = event.occupancy;
        console.info("Presence actions: " + action);
        if (action === "join") {
          console.log(uuid + ": Join");
        }
        else if (action === "timeout") {
          // TODO
        }
        else if (action === "leave") {
          // TODO
          console.log(uuid + ": timeout");
        }
        else if (action === "state-change") {
          // TODO
          console.log("!!!@@@ state-change event @@@!!!");
        }
        console.groupEnd();
      }
    };
  }

  // componentWillMount() {
  //   console.log(this);
  //   this.setState({
  //     messages: [
  //       {
  //         _id: 1,
  //         text: 'Hello developer',
  //         createdAt: new Date(),
  //         user: {
  //           _id: 2,
  //           name: 'React Native'
  //         }
  //       },
  //       {
  //         _id: 2,
  //         text: 'Hello 2',
  //         createdAt: new Date(),
  //         user: {
  //           _id: 1,
  //           name: 'React Native'
  //         }
  //       },
  //       {
  //         _id: 3,
  //         text: 'Hello 3',
  //         createdAt: new Date(),
  //         user: {
  //           _id: 2,
  //           name: 'React Native'
  //         }
  //       }
  //     ],
  //   });
  // }

  // componentWillUnmount() {
  //   this.pubnub.unsubscribe({
  //     channelGroups: [stores.find(i => i[0] === 'pubnubUserId')[1] + '-channelgroup'],
  //   });
  // }

  getUserChatHistory(groupChatID) {

    console.log("History groupChatID ");
    console.log(groupChatID);
    var param = {
      "limit": 100,
      "order": "DESCENDING"
    };
    fetch(GATEWAY_URI + "/messaging/groupChats/" + groupChatID + "/messages/search", {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization': "Bearer " + this.token
      },
      body: JSON.stringify(param)
    })
      // .then((response) => {
      //   return response.json()
      // })
      .then((responseJSON) => {
        // //var msges = responseJSON.messages;
        // console.log("chat History");
        // console.log(responseJSON);
        // if (responseJSON._bodyText != '') {
        //   var msges = JSON.parse(responseJSON._bodyText).messages;
        //   //var newChatHistory = this.state.chatHistory.slice();
        //   var newChatHistory = JSON.parse(JSON.stringify(this.state.chatHistory));
        //   console.log(msges.length);
        //   for (var i = 0; i <= msges.length - 1; i++) {
        //     var message = msges[i]
        //
        //     if (message.body.indexOf("InvitationToGroupChat") > -1) {
        //       continue;
        //     }
        //
        //     newChatHistory.push({
        //       timetoken: message.time,
        //       sender: message.sender,
        //       message: message.body
        //     });
        //   }
        //
        //   this.setState({
        //     chatHistory: newChatHistory
        //   });
        // }
        // // this.forceUpdate()
        // console.log("message Bind one")
        // console.log(this.state.chatHistory)

        if (responseJSON._bodyText != '') {
          var msges = JSON.parse(responseJSON._bodyText).messages;
          //var newChatHistory = this.state.chatHistory.slice();
          var newChatHistory = JSON.parse(JSON.stringify(this.state.chatHistory));
          console.log("Rajish");
          console.log(msges);
          var newChatHistory = JSON.parse(JSON.stringify(this.state.chatHistory));

          for (var i = 0; i <= msges.length - 1; i++) {
              var message = msges[i]

              if (message.body.indexOf("InvitationToGroupChat") > -1) {
                continue;
              }

              newChatHistory.push({
                _id: message.time,
                text: message.body,
                createdAt: new Date(message.time / 10000000),
                user: {
                  _id: message.sender === this.pubnubUserId ? 1 : 2,
                  name: 'React Native'
                }
              });
            }

            this.setState({
              messages: newChatHistory.reverse()
            });
        }

      }).catch((error) => {
        console.error(error);
      });
  }

  // bindMsgToUI(unixtime, text, fromUser) {
  //   console.groupCollapsed("bindMsgToUI");
  //   console.log(unixtime);
  //   console.log(text);
  //   console.log(fromUser);
  //   console.groupEnd();
  //   var mstime = this.convertTimeTokenToTime(unixtime);
  //   if (text === "newchatroom") {
  //     return (
  //       <View style={styles.emptyChatRoom}>
  //         <Text> you just have created a chatroom, add users to discuss topic!!</Text>
  //       </View>
  //     );
  //   }
  //   else {
  //     if (chatHistoryDate != mstime.day) {
  //       chatHistoryDate = mstime.day;
  //       return (
  //         <View style={styles.HistoryDateSeperator}>
  //           <Text> {mstime.day}</Text>
  //         </View>
  //       );
  //     }
  //     if (fromUser == this.pubnubUserId) {
  //       console.log(fromUser);
  //       return (
  //         <View style={styles.TextRight}>
  //           <Text> </Text>
  //           <Text> {mstime.time}</Text>
  //           <Text> {text} </Text>
  //         </View>
  //       );
  //     }
  //     else {
  //       console.log(fromUser);
  //       return (
  //         <View style={styles.TextLeft}>
  //           <Text> {fromUser}</Text>
  //           <Text> {mstime.time}</Text>
  //           <Text> {text}</Text>
  //         </View>
  //       );
  //     }
  //   }
  // }

  sendMessage(cmsg) {

    console.log("In sendMessage");
    console.log(cmsg[0].text);
    var chatId = this.props.navigation.state.params.grpChatId;
    var chatURL = "/messaging/groupChats/" + chatId + "/messages"
    var message = {
      body: cmsg[0].text,
      tags: [
        {
          tagValue: "string",
          tagName: "string"
        }
      ]
    };

    return fetch(GATEWAY_URI + chatURL, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization': 'Bearer ' + this.token
      },
      body: JSON.stringify(message)
    })
    .then((responseJSON) => {
      console.log("Send message success");
      console.log(responseJSON);
      this.getUserChatHistory(this.props.navigation.state.params.grpChatId);
    }).catch((error) => {
      console.error(error);
      Alert.alert(JSON.stringify(error.message));
    });

    // var newChatHistory = JSON.parse(JSON.stringify(this.state.message));
    // newChatHistory.push({
    //   _id: Date.now(),
    //   text: cmsg[0].text,
    //   createdAt: Date.now(),
    //   user: {
    //     _id: 1,
    //     name: 'React Native'
    //   }
    // });
    //
    // this.setState({
    //   messages: newChatHistory
    // });

    // this.setState((previousState) => ({
    //   messages: GiftedChat.append(previousState.messages, cmsg),
    // }));

    // this.getUserChatHistory(props.navigation.state.params.grpChatId);

  }

  render() {
    var { params } = this.props.navigation.state;

    var chatUi =
      this.state.chatHistory.map((message, i) =>
        <ChatUI
          key={i}
          timetoken={message.timetoken}
          message={message.message}
          sender={message.sender}>
        </ChatUI>
      );

      return (
        <Container>
          <GiftedChat
            messages={this.state.messages}
            onSend={(message) => this.sendMessage(message)}
            user={{
              _id: 1,
            }}
          />
          {Platform.OS === 'android' ? <KeyboardSpacer /> : null }
        </Container>
      );
    // return (
    //   <KeyboardAvoidingView behavior="padding" style={styles.container}>
    //     <View style={styles.header}>
    //       <Text style={styles.headerText}> {params.name} </Text>
    //     </View>
    //
    //     <View Style={styles.contentContainer}>
    //
    //
    //       {chatUi}
    //       </View>
    //
    //     <View style={styles.chatInputBox}>
    //       <TextInput
    //         style={styles.inputBox}
    //         placeholder="Type your message here"
    //         onChangeText={(message) => this.setState({ message })}
    //         value={this.state.text}
    //       />
    //       <Button
    //         title="Send message"
    //         color="#841584"
    //         accessibilityLabel="Send message button"
    //         onPress={() => this.sendMessage(this.state.message)}
    //       />
    //     </View>
    //
    //     {/* <Text> {params.userId }</Text> */}
    //
    //   </KeyboardAvoidingView>
    // );
  }
}



var ChatUI = React.createClass({


  convertTimeTokenToTime(timetoken) {
    var timestmp = (timetoken) / 10000000
    var t = new Date(timestmp * 1000);
    var hday = t.getDate() + " - " + (t.getMonth() + 1) + " - " + t.getFullYear();
    var hours = t.getHours();
    var minutes = t.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return { "time": strTime, "day": hday };
  },

  render() {

    var mstime = this.convertTimeTokenToTime(this.props.timetoken);
    if (this.props.message === "newchatroom") {
      return (
        <View style={styles.emptyChatRoom}>
          <Text> you just have created a chatroom, add users to discuss topic!!</Text>
        </View>
      );
    }
    else {


      if (chatHistoryDate != mstime.day) {
        chatHistoryDate = mstime.day;
        return (
          <View style={styles.HistoryDateSeperator}>
            <Text> {mstime.day}</Text>
          </View>
        );
      }
      if (this.props.sender == this.pubnubUserId) {
        console.log(this.props.sender);
        return (
          <View style={styles.TextRight}>
            <Text> </Text>
            <Text> {mstime.time}</Text>
            <Text> {this.props.message} </Text>
          </View>
        );
      }
      else {

        return (
          <View style={styles.TextLeft}>
            <Text> {this.props.sender}</Text>
            <Text> {mstime.time}</Text>
            <Text> {this.props.message}</Text>
          </View>
        );
      }
    }


  }

})

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    height:'100%'
  },

  header: {
    width: '100%',
    height: 70,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center'
  },

  headerText: {
    color: 'white',
    alignItems: 'center',
    fontSize: 28,

  },
  chatInputBox: {
    width: '100%',
    height: '90%',
    flexDirection: 'column',
  },
  inputBox: {
    width: '100%',
    flexDirection: 'column',
  },
  chatData: {
    height: '50%',
    backgroundColor: 'cornflowerblue',
    flex:1
  },
  TextRight: {
    height: 100,
   // width:40,
    backgroundColor: 'white',
    alignItems: 'flex-end',
    paddingVertical:10,
  },
  TextLeft: {
    height: 100,
   // width:40,
    backgroundColor: 'white',
    alignItems: 'flex-start',
    paddingVertical:10,
  },
  contentContainer: {

    height:'100%',
    backgroundColor: 'cornflowerblue',
  }
});
