import React, { Component } from 'react';

import { AsyncStorage, Alert, Image, Platform, StyleSheet, KeyboardAvoidingView } from 'react-native';
import { Container, Content, Button, Text, Item, Input, Form } from 'native-base';
import KeyboardSpacer from 'react-native-keyboard-spacer';
//
// import {
//   Alert,
//   AsyncStorage,
//   Button,
//   Image,
//   KeyboardAvoidingView,
//   StyleSheet,
//   Text,
//   TextInput,
//   View
// } from 'react-native';

import ChatScreen from './ChatScreen';

const GATEWAY_URI = 'http://satdevv713.athenahealth.com:38888';
const CLIENT_ID = 'YAJcO0EfQ9IvngIuXA7d';

export default class LoginForm extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);

    this.state = {
      username: '', //'jnishant@athenahealth.com',
      password : '', //'July@2017',
      token: undefined,
      loggedIn: false
    };

    this.userLogin = this.userLogin.bind(this);
  }

  componentWillMount() {
    var needLogin = false;

    const requiredStorageKeys = [
      'subscribeKey',
      'publishKey',
      'cipherKey',
      'token',
      'pubnubUserId'
    ];

    AsyncStorage.getAllKeys((err, keys) => {
      requiredStorageKeys.forEach((key) => {
        if(keys.indexOf(key) < 0) needLogin = true;
      });

      if(!needLogin) {
        const { navigate } = this.props.navigation;
        navigate('UserListScreen', {});
      }
    });
  }

  userLogin() {
    if(!(this.state.username && this.state.password)) {
      return Alert.alert('Input required!');
    }

    return fetch(GATEWAY_URI + '/login/login', {
      method: 'GET',
      headers: {
        'username': this.state.username,
        'password': this.state.password,
        'client-id': CLIENT_ID,
        'scopes': 'openid,profile'
      }
    }).then((response) => {
       return response.json()
    }).then((responseJSON) => {
      if(responseJSON.token == '') {
        Alert.alert("Please enter valid credentials");
      }
      else {
        Promise.all([
          AsyncStorage.setItem('token', responseJSON.token),
          this.initPubnubConfig(responseJSON.token),
          this.initChannel(responseJSON.token)
        ]).then(() => {
          var {navigate} = this.props.navigation;
          navigate('UserListScreen', {});
        });
      }
    }).catch((error) => {
      Alert.alert(JSON.stringify(error.message));
    });
  }

  initPubnubConfig(token) {
    return fetch(GATEWAY_URI + '/public/messaging/config', {
      method:'GET',
      headers:{
        'Accept':'application/json',
        'Authorization': token
      }
    })
    .then((response) => {
      return response.json()
    })
    .then((responseJSON) => {
      return Promise.all([
        AsyncStorage.setItem('subscribeKey', responseJSON.subscribeKey),
        AsyncStorage.setItem('publishKey', responseJSON.publishKey),
        AsyncStorage.setItem('cipherKey', responseJSON.cipherKey)
      ]);
    }).catch((error) => {
      console.error(error);
      Alert.alert(JSON.stringify(error.message));
    });
  }

  initChannel(token) {
    return fetch(GATEWAY_URI + '/messaging/channels', {
      method:'GET',
      headers:{
        'Accept':'application/json',
        'Authorization': 'Bearer ' + token
      }
    })
    .then((response) => {
      return response.json()
    })
    .then((responseJSON) => {
      return AsyncStorage.setItem('pubnubUserId', responseJSON.subscriptions[0].split("-")[0]);
    }).catch((error) => {
      console.error(error);
      Alert.alert(JSON.stringify(error.message));
    });

  }

  // TODO: Remove. This is a test function.
  logStorage() {
    AsyncStorage.getAllKeys((err, keys) => {
      AsyncStorage.multiGet(keys, (err, stores) => {
        console.log('Storage');
        console.log(stores);
      });
    });
  }

  // TODO: Remove. This is a test function.
  clearStorage() {
    AsyncStorage.clear((err, keys) => {
      console.log("Cleared storage");
    });
  }

  // render() {
  //   return (
  //     <KeyboardAvoidingView behavior="padding" style={styles.container}>
  //       <Image style={styles.loginBanner} source={require('../assets/images/login.png')} />
  //
  //       <View style={styles.container}>
  //         <TextInput
  //           style={styles.textInput}
  //           returnKeyType="next"
  //           onSubmitEditing={()=>this.passwordInput.focus()}
  //           autoCapitalize='none'
  //           autoCorrect={false}
  //           placeholder="Username"
  //           onChangeText={(username) => this.setState({ username })}
  //         />
  //         <TextInput
  //           onChangeText={(password) => this.setState({ password })}
  //           secureTextEntry={true}
  //           style={styles.textInput}
  //           returnKeyType="go"
  //           placeholder="Password"
  //           ref={(input)=>this.passwordInput = input}
  //         />
  //         <Button
  //           title="Login"
  //           color="#841584"
  //           accessibilityLabel="Login button"
  //           onPress={this.userLogin}
  //         />
  //         <Button
  //           title="Log storage"
  //           color="#841584"
  //           accessibilityLabel="Log storage button"
  //           onPress={this.logStorage}
  //         />
  //         <Button
  //           title="Clear storage"
  //           color="#841584"
  //           accessibilityLabel="Clear storage button"
  //           onPress={this.clearStorage}
  //         />
  //       </View>
  //     </KeyboardAvoidingView>
  //   );
  // }

  render() {
    return (
      <Container style={styles.screenContainer}>
        <Container style={styles.bannerContainer}>
          <Image style={styles.loginBanner} source={require('../assets/images/login.png')} />
        </Container>
        <Container style={styles.inputContainer}>
          <Content>
            <Form>
              <Item regular style={styles.inputFields}>
                <Input
                  keyboardType='email-address'
                  placeholder='Email'
                  onChangeText={(username) => this.setState({ username })}
                />
              </Item>
              <Item regular style={styles.inputFields}>
                <Input
                  placeholder='Password'
                  secureTextEntry
                  onChangeText={(password) => this.setState({ password })}
                />
              </Item>
              <Button
                full
                style={styles.inputFields}
                 onPress={this.userLogin}
              >
                <Text>Login</Text>
              </Button>
            </Form>
          </Content>
        </Container>
        {Platform.OS === 'android' ? <KeyboardSpacer /> : null }
      </Container>
    );
  }
}


const styles = StyleSheet.create({
  screenContainer: {
    flex: 1
  },
  bannerContainer: {
    flex: 1
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    padding: 20,
    flex: 1
  },
  inputFields: {
    marginTop: 10
  },
  loginBanner: {
    alignItems:'center',
    justifyContent:'center',
    width:'100%',
    height:'100%'
  }
});
// const styles = StyleSheet.create({
//   container: {
//     alignItems: 'center'
//   },
//   textInput: {
//     width: '90%',
//     height: 40,
//     paddingHorizontal:10,
//     marginBottom:20
//   },
//   loginBanner: {
//     alignItems:'center',
//     justifyContent:'center',
//     width:'100%',
//     height:'60%'
//   }
// });
