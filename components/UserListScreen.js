import React, { Component } from 'react';
import PubNubReact from 'pubnub-react';
import { StackNavigator } from 'react-navigation';
import { AsyncStorage, Text, View, FlatList, StyleSheet, Image,Alert } from 'react-native';

const GATEWAY_URI = 'http://satdevv713.athenahealth.com:38888';
const users = [
  { "name": "Ren", "pubnubId": "ravisingh_athenahealth_com" },
  { "name": "John", "pubnubId": "smohanty_athenahealth_com" },
  { "name": "Nishant", "pubnubId": "jnishant_athenahealth_com" },
  { "name": "Anna", "pubnubId": "smariya_athenahealth_com" },
  { "name": "Brandon", "pubnubId": "sgoje_athenahealth_com" },
  { "name": "Michael", "pubnubId": "bgupta_athenahealth_com" },
];

export default class ChatPage extends Component {

    constructor(props){
        super(props);

        AsyncStorage.getAllKeys((err, keys) => {
          AsyncStorage.multiGet(keys, (err, stores) => {


            this.token = stores.find(i => i[0] === 'token')[1];
            this.pubnubUserId = stores.find(i => i[0] === 'pubnubUserId')[1];
            this.stores = stores;
            this.onPressUser = this.onPressUser.bind(this);


          });
        });


    }
  onPressUser(prop,i){
console.log("this.pubnubUserId")
console.log(this.pubnubUserId)

console.log("users[i].pubnubId")
console.log(users[i].pubnubId)

    var groupChatInput = {
      "userIds": [this.pubnubUserId, users[i].pubnubId],
      'name': 'test',
      'description': 'test desc'
  };
     fetch(GATEWAY_URI+ "/messaging/groupChats",{
      method:"POST",
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization': "Bearer " + this.token
      },
      body: JSON.stringify(groupChatInput)

    })
    // .then((response)=>{
    //   return response.json()
    // })
    .then((responseJSON)=>{

      if (responseJSON._bodyText != '') {
        var resp = JSON.parse(responseJSON._bodyText)
        console.log("user responseJSON")
        console.log(resp)
      const { navigate } = this.props.navigation;
      navigate('ChatScreen', {title: users[i].name, userId:users[i].pubnubId, name:users[i].name,grpChatId: resp.groupChatId});
      }
    })
    .catch((error) => {
      console.error(error);
    });

  }

    render() {
        ulist = [];

        for (let i = 0; i < users.length - 1; i++) {
          ulist.push(
            <View style={styles.userWrapper}>
              <View style={styles.user} >
                <Image style={styles.userAvatar} source={require('../assets/images/user_account.png')} />
                <Text
                style={styles.userName}
                pubnubId ={users[i].pubnubId}
                onPress={(pubnubId) => this.onPressUser(this,i)}

                 > {users[i].name} </Text>
              </View>
              <View style={styles.LastChatTimeContainer} >
                <Text style={styles.LastChatTime} > 2 days ago </Text>
              </View>
            </View>
          );
        }

        return (
          <View style={styles.container}>
            {/* <List>
          <FlatList
            data={this.state.data}
            renderItem={({ item }) => (
              <ListItem
                roundAvatar
                title={`${item.name} ${item.name}`}
                subtitle={item.name}

              />
            )}
          />
          </List> */}
            <View style={styles.UserListContainer}>
              {ulist}
            </View>
          </View>
        );
      }
}

const styles = StyleSheet.create({
    container: {
      alignItems: 'center'
    },
    textInput: {
      width: '90%',
      height: 40,
      paddingHorizontal: 10,
      marginBottom: 20
    },

    user: {
      alignItems: 'center',
      justifyContent: 'flex-start',
      height: 70,
      width: '100%',
      fontSize: 25,

      paddingLeft: 28,
      flexDirection: 'row',
    },
    userWrapper: {
      borderBottomColor: 'black',
      borderBottomWidth: 0.5,
    },
    UserListContainer: {
      paddingLeft: 8,
      width: '100%'

    },
    userAvatar: {
      width: 45,
      height: 45,

    },
    userName: {
      paddingLeft: 28,
      alignItems: 'flex-start',
      justifyContent: 'center'
    },
    LastChatTime: {
      paddingRight: 8,
      alignItems: 'center',
      fontSize: 10

    },
    LastChatTimeContainer: {

      paddingRight: 8,
      alignItems: 'flex-end',
      fontSize: 10

    }

  });
